{-# LANGUAGE FlexibleContexts #-}

module Main where

import Control.Eff
import Control.Eff.Reader.Lazy
import Control.Eff.Writer.Lazy
import Control.Eff.State.Lazy
import Data.Void

-- Pure computation
testPure :: Int 
testPure = run test
  where
    test :: Eff Void Int
    test = return 42

-- Reader Effect
testReader :: Float
testReader = run $ runReader (runReader test (10 :: Int)) (20 :: Float)
  where
    test :: (Member (Reader Int) r, Member (Reader Float) r) => Eff r Float
    test = do
      v1 <- ask
      v2 <- ask
      return $ fromIntegral (v1 + (1 :: Int)) + (v2 + (2 :: Float))

-- State Effect
testState :: Int -> Int
testState x = run $ execState (x :: Int) test 
  where
    test :: Member (State Int) r => Eff r ()
    test = modify (* (2 :: Int))

logNumber :: Member (Writer [String]) r => Int -> Eff r Int  
logNumber x = tell (["Got number: " ++ show x]) >> return x

-- Writer Effect
testWriter :: ([String], Int)
testWriter = run $ runMonoidWriter test
  where
    test :: Member (Writer [String]) r => Eff r Int
    test = do 
      a <- logNumber 3  
      b <- logNumber 5 
      tell ["Gonna multiply these two"] 
      return (a*b) 

-- Mixed Effects
-- не пашет :(
testMixed :: ([String], Int)
testMixed = run $ runReader (runMonoidWriter test) "Hi, bro!"
  where
    test :: (Member (Reader String)   r, 
             Member (Writer [String]) r) => Eff r Int
    test = do
      initMsg <- ask
      a <- logNumber (3 :: Int)
      b <- logNumber (5 :: Int)
      tell ["Gonna multiply these two"]
      return (a*b) 