# Исследование возможностей библиотеки Extensible Effects

Статья: "Extensible Effects: An Alternative to Monad Transformers" // http://okmij.org/ftp/Haskell/extensible/exteff.pdf"

Пакет на Hackage: https://hackage.haskell.org/package/extensible-effects-1.9.0.1